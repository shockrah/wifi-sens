/* Promiscuous Front facing library for dealing with promiscuous mode ESP8266 wifi packets
 * 
 * Purpose: this library aims to provide some simple packet parsing utilities so that you don't have to write 
 * them all yourself.(also cuz i like messing with this stuff so its useful to me xd)
 * Author: shockrah
 */
#include <ESP8266WiFi.h>
#include "promiscuous.h"
#include "struct.h"
#include "wifistrings.h"

/* Default IO func handler */
void default_io( const wifi_promiscuous_pkt_t* prom_pkt,
	const wifi_ieee80211_pkt_t* ieee_pkt, 
	const wifi_ieee80211_mac_hdr_t* mac_hdr, 
	const uint8_t* data) {

	/* mac addres buffers */
	char addr[] = "000000000000\0";	// receiver mac addr 
	const size_t mac_len = strlen(addr);

	// print_mac(char*, const size_t, uint8_t) comes ffrom the wifistrings module
	print_mac(addr, mac_len, mac_hdr->addr1);
	Serial.print(" ");
	print_mac(addr, mac_len, mac_hdr->addr2);
	Serial.print(" ");
	Serial.print((int)prom_pkt->rx_ctrl.rssi);
	Serial.print(" ");
}
void promiscuous_payload_p(const wifi_promiscuous_pkt_t* ptr) {
	// we'll be sending this output to a binary log file so dealing with raw binary data shouldn't be an issue
	// ptr.rx_ctrl.legacy_length give us the payload length of the packet
	unsigned pay_len = (unsigned)ptr->rx_ctrl.legacy_length;
	
	if(!pay_len) {return;}
	
	Serial.print("Payload length: "); Serial.println(pay_len);
	for(unsigned i =0;i<pay_len;i++) {
		Serial.print(ptr->payload[i],HEX);
		//Serial.print(ptr->payload[i]);
	}
	Serial.println("\n---");
}
/* Uses DEFAULT_HANDLER as its object and deals with other important things which must be setup */ 
void default_handler(uint8_t *buffer, uint16_t len) {
	/* First we'll setup any pointers we may need */

	const wifi_promiscuous_pkt_t* prom_ptr	= (wifi_promiscuous_pkt_t *)buffer;
	
	/* Pointer to promiscuous packet's payload which will be our IEEE 802.11 packet*/
		/* At this point we're looking data from our transport layer */
	const wifi_ieee80211_pkt_t* ieee_pk		= (wifi_ieee80211_pkt_t *)prom_ptr->payload; 

	/* IEEE MAC header pointer */
	const wifi_ieee80211_mac_hdr_t* mac_hdr	= &ieee_pk->hdr;

	/* IEEE packet payload pointer */
	const uint8_t* data						= ieee_pk->payload;

	/* frame control is inside the packet header */
	const wifi_header_frame_control_t *frame_ctrl = (wifi_header_frame_control_t *) &mac_hdr->frame_ctrl;

	/* Passing off any initialized data off to our output function */
	default_io(prom_ptr, ieee_pk, mac_hdr, data);
	promiscuous_payload_p(prom_ptr);
}


void prom_set_handler(void (*func)(uint8_t*, uint16_t)) {
	if(func != NULL) {
		wifi_set_promiscuous_rx_cb(func);
	}
	else {
		wifi_set_promiscuous_rx_cb(default_handler);
	}
}
