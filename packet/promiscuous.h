/* Interface for setting up promiscuous mode on ESP8266 chips */
#include "struct.h"
#include "wifistrings.h"

/* Default IO func for reading out data from wifi sniff */
void default_io(const wifi_promiscuous_pkt_t* prom_pkt, 
    const wifi_ieee80211_pkt_t* ieee_pkt,
    const wifi_ieee80211_mac_hdr_t* mac_hdr,
    const uint8_t* data
);

/* gives back binary of the promiscuous packet's payload */
void promiscuous_payload_p(const wifi_promiscuous_pkt_t* ptr);

/* main handler which basically just feeds data into a structure */
void default_handler(uint8_t* buffer, uint16_t* len);

// 
void prom_set_handler(void (*func)(uint8_t*, uint16_t));