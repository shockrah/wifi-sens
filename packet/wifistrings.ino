#include "wifistrings.h"
#include "struct.h"

/* we're assuming that our formatted string is in the proper format to begin with */
static void convert_mac(char* buffer, const size_t bsize, const uint8_t* data) {
	if(bsize != MAC_BUFFER_SIZE) {
		Serial.print("bad buffer");
		return;
	}
	for(int i=0;i<6;i++) {
		sprintf(&buffer[i*2], "%02x", data[i]);
	}
}

void print_mac(char* buffer, const size_t bsize, const uint8_t* data) {
	/* converting the printing the mac address without a newline */
	if(bsize != MAC_BUFFER_SIZE) {
		Serial.print("bad buffer");
		return;
	}
	convert_mac(buffer, bsize, data);
	Serial.print(buffer);
	return;
}

int signal_strength(const wifi_promiscuous_pkt_t* pkt) {
	return pkt->rx_ctrl.rssi;
}


/* Return wifi packet type in printable string form */
char* wifi_packet_type_str(const wifi_promiscuous_pkt_type_t type, const wifi_mgmt_subtypes_t subtype) {
	switch(type) {
		case WIFI_PKT_MGMT:
			switch(subtype) {
				case ASSOCIATION_REQ:
					return "ASSOCIATION_REQ";
				case ASSOCIATION_RES:
					return "ASSOCIATION_RES";
				case REASSOCIATION_REQ:
					return "REASSOCIATION_REQ";
				case REASSOCIATION_RES:
					return "REASSOCIATION_RES";
				case PROBE_REQ:
					return "PROBE_REQ";
				case PROBE_RES:
					return "PROBE_RES";
				case NU1:  /* ......................*/
					return "NU1";
				case NU2:  /* 0110, 0111 not used */
					return "NU2";
				case BEACON:
					return "BEACON";
				case ATIM:
					return "ATIM";
				case DISASSOCIATION:
					return "DISASSOCIATION";
				case AUTHENTICATION:
					return "AUTHENTICATION";
				case DEAUTHENTICATION:
					return "DEAUTHENTICATION";
				case ACTION:
					return "ACTION";
				case ACTION_NACK:
					return "ACTION_NACK";
				default:
					return "UNSUPPORTED";
			}
		case WIFI_PKT_CTRL:
			return "CONTROL";
		case WIFI_PKT_DATA:
			return "DATA";
		case WIFI_PKT_MISC:
			return "MISC";
		default:
			return "UNSUPPORTED";
	}
}
